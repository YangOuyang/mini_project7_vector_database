# mini_project7_vector_database

## Initial Setup
1. Create the rust project `cargo new project_name && cd project_name`
2. Download the latest Qdrant image from Dockerhub docker pull qdrant/qdrant and run it with grpc enabled
```bash
docker pull qdrant/qdrant
# With env variable
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```
3. ![alt text](images/qdrant_start.png)
4. Add the following dependencies to the `Cargo.toml` file
```toml
[dependencies]
anyhow = "1.0.81"
qdrant-client = "1.8.0"
serde_json = "1.0.114"
tokio = { version = "1.36.0", features = ["rt-multi-thread"] }
tonic = "0.11.0"
rand = "0.8"
serde = "1.0.197"
uuid = "0.8.2" # Check for the latest version
```
5. Edit the `main.rs` file to create a simple web server
```rust
// Ingest data into Vector database with 10-dimension vector and payload
let mut rng = rand::thread_rng();
let points: Vec<PointStruct> = (1..=20)
    .map(|id| {
        let vector: Vec<f32> = (0..10).map(|_| rng.gen_range(0.0..100.0) as f32).collect();
        let payload: Payload = json!({
            "name": format!("Point {}", id),
            "value": rng.gen_range(1..100),
        })
        .try_into()
        .unwrap();

        PointStruct::new(id, vector, payload)
    })
    .collect();
client
    .upsert_points_blocking(collection_name, None, points, None)
    .await?;

// Perform queries and aggregations with vector [1.0; 10]
let search_result = client
    .search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: vec![1.0; 10], // This is arbitrary; adjust as needed
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    })
    .await?;

// Simple visualization: Print details of each point
println!("Found points details:");
for point in search_result.result {
    println!(
        "ID: {:?}, Payload: {:?}, Score: {}",
        point.id, point.payload, point.score
    );
}
```

## Visulization of the Vector Database
1. Go to the `http://localhost:6333/dashboard` to see the web UI of the qdrant vector database
2. Click the `Add collection` button to create a new collection
3. Run the visualization command to show the vector database
2. ![alt text](images/qdrant_dashboard.png)
