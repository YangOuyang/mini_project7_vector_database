use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{CreateCollection, SearchPoints, VectorParams, VectorsConfig};
use rand::Rng;
use serde_json::json;

#[tokio::main]
async fn main() -> Result<()> {
    // Initialize the top-level client to connect to Qdrant on localhost
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    let collection_name = "qdrant_test";
    client.delete_collection(collection_name).await?;

    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 10,
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    // Insert multiple points with varying data
    let mut rng = rand::thread_rng();
    let points: Vec<PointStruct> = (1..=20)
        .map(|id| {
            // Convert each element of the vector from f64 to f32
            let vector: Vec<f32> = (0..10).map(|_| rng.gen_range(0.0..100.0) as f32).collect();
            let payload: Payload = json!({
                "name": format!("Point {}", id),
                "value": rng.gen_range(1..100),
            })
            .try_into()
            .unwrap();

            PointStruct::new(id, vector, payload)
        })
        .collect();
    client
        .upsert_points_blocking(collection_name, None, points, None)
        .await?;

    // Perform a search
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![1.0; 10], // This is arbitrary; adjust as needed
            limit: 5,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

    // Simple visualization: Print details of each point
    println!("Found points details:");
    for point in search_result.result {
        println!(
            "ID: {:?}, Payload: {:?}, Score: {}",
            point.id, point.payload, point.score
        );
    }

    Ok(())
}
